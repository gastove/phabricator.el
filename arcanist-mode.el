;;; arcanist-mode.el --- A major mode for editing arc diff messages
;;
;;; Commentary:
;;  Arc is a pile of shit. Maybe this will help.
;;; Code:

(require 'phabricator)
(require 'company)
(require 'dash)
(require 's)
(require 'cl-lib)

(defvar arc/realname-username-pairs nil)

(defun arc/parse-pairs-from-response (resp)
  "Parse realname-username pairs from RESP."
  (->> resp
       (phab/data-from-response)
       (-map (lambda (user)
               (-let* ((user-data (alist-get 'fields user))
                       (username (alist-get 'username user-data))
                       (realname (alist-get 'realName user-data)))
                 `(,realname . ,username))))
       (setq arc/realname-username-pairs)))

(defun arc/parse-diffs (resp)
  (->> resp
       (phab/data-from-response)
       (-map (lambda (diff)
               (-let* ((diff-id (alist-get 'id diff))
                       (diff-data (alist-get 'fields diff))
                       (title (alist-get 'title diff-data))
                       (author-phid (alist-get 'authorPHID diff-data)))
                 (list diff-id title author-phid))))))

(defun arc/load-realname-username-pairs ()
  "Call the phabricator API synchronously and parse the result."
  (arc/parse-pairs-from-response (phab/user-search nil nil 't)))

(defun arc/get-usernames ()
  (-map
   (lambda (pair)
     (-let [rname (cdr pair)]
       (propertize rname :realname (car pair))))
   arc/realname-username-pairs))

(defun arc/load-or-get-usernames ()
  (unless arc/realname-username-pairs
    (arc/load-realname-username-pairs))
  (arc/get-usernames))

(defun arc/company-candidates (arg)
  (-let [usernames (arc/load-or-get-usernames)]
    (if arg
        (-filter
         (lambda (haystack) (s-contains? arg haystack))
         usernames)
      usernames)))

(defun arc/company-backend-prefix ()
  (when (looking-back "\\s-@\\w+")
    (company-grab-symbol "\\s-@\\w+")))

(defun arc/company-annotate-with-realname (arg)
  (format " -> %s" (get-text-property 0 :realname arg)))

(defun arc/company-arc-username-backend (command &optional arg &rest ignore)
  (interactive (list 'interactive))
  (cl-case command
    (interactive (company-begin-backend 'arc/company-arc-backend))
    (prefix (and (eq major-mode 'arcanist-mode)
                 (company-grab-symbol-cons "\\s-@\\w+")))
    (candidates (arc/company-candidates arg))
    (annotation (arc/company-annotate-with-realname arg))))

;;;#autoload
(add-to-list 'company-backends 'arc/company-arc-username-backend)


(define-derived-mode arcanist-mode gfm-mode "Arcanist" "Major mode for arcanist buffers")

(provide 'arcanist-mode)
;;; arcanist-mode.el ends here
