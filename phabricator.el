;;; phabricator.el --- Phabricator Integration -*- lexical-binding: t -*-
;;
;;; Commentary:
;;  Work with phabricator a little less bad
;;; Code:

;; https://tkf.github.io/emacs-request/

;; Go here!
;; (require 'pg)

;; (with-pg-connection con '("phabricator" "machinist")
;;   (pg:result
;;    (pg:exec con "select * from diffs where status not in ('Closed', 'Abandoned') limit 5")
;;    :tuples))

(require 'request)
(require 'request-deferred)
(require 'concurrent)
(require 'dash)
(require 's)

;; Revision docs http://phabricator.local.disqus.net/conduit/method/differential.revision.search/

;; Possible parameters to the search API

;; ids  IDs list<int>   Search for objects with specific IDs.
;; phids    PHIDs   list<phid>  Search for objects with specific PHIDs.
;; responsiblePHIDs Responsible Users   list<string>    Find revisions that a given user is responsible for.
;; authorPHIDs  Authors list<user>  Find revisions with specific authors.
;; reviewerPHIDs    Reviewers   list<string>    Find revisions with specific reviewers.
;; repositoryPHIDs  Repositories    list<string>    Find revisions from specific repositories.
;; status   Status      Not supported.
;; subscribers  Subscribers list<user>  Search for objects with certain subscribers.
;; projects Tags    list<project>   Search for objects tagged with given projects.
;; bucket   Bucket      Not supported.

(defvar phab/phabricator-url "http://phabricator.local.disqus.net/api")
(defvar phab/token-pair '("api.token" . "cli-6esinuefkcxl76c54qoyo32nrhvy"))

(defvar phab/search-fields
  '(("ids" . "list int")
    ("phids" . "list phid")
    ("responsiblePHIDs" . "list string")
    ("authorPHIDs" . "list user") ;; wtf is a "user"
    ("repositoryPHIDs" . "list string")
    ("subscribers" . "list user") ;; same question
    ("projects" . "list project")))

(defvar phab/query-result nil)
(defvar phab/debug nil)

(defmacro phab/create-query-success-fn (result-var)
  ;; (setq phab/debug "honk honk")
  `(function*
    (lambda (&key data &allow-other-keys)
      (set ,result-var data))))

(defun* phab/query-error-fn (&key error-thrown &allow-other-keys&rest _)
  (message "Got error: %S" error-thrown))

(defun dotted-pairp (candidate)
  (and (cdr candidate) (atom (cdr candidate))))

(defun phab/resolve-query (&optional query)
  ;; (cond
  ;;  ((and query (dotted-pairp query)) query)
  ;;  (query (phab/compile-query query))
  ;;  ((not query) '("queryKey" . "active")))
  (if query
      (phab/compile-query query)
    '("queryKey" . "active"))
  )

(defun phab/generate-php-nested-associative-array-params (array-name data)
  "Build a PHP associative array ARRAY-NAME from alist DATA.
Return pairs of (associate-array . val) for use in cURL.

E.G:

curl http://phabricator.local.disqus.net/api/differential.revision.search \
   -d api.token=api-token \
   -d constraints[phids][0]=PHID-USER-3r55zlikd6ctuvcvayf7 \
   -d constraints[authorPHIDs][0]=PHID-USER-3r55zlikd6ctuvcvayf7 \
   -d constraints[authorPHIDs][1]=PHID-USER-3r55zlikd6ctuvcvayf5
"
  (-let ((res '())
         (idx -1)
         (prev nil)
         (tpl "%s[%s][%i]"))
    (dolist (elem data)
      (setq prev (car elem))
      (dolist (val (cdr elem))
        (-let ((item (car elem)))
          ;; Increment index if we're in the same key, else reset
          (if (eq prev item)
              (setq idx (1+ idx))
            (setq idx 0))
          (add-to-list 'res `(,(format tpl array-name item idx) . ,val))
          ))
      )
    (reverse res)))

(defun phab/generate-php-indexed-associative-array-param (array-name data)
  (-let ((res '())
         (idx -1)
         (tpl "%s[%i]"))
    (dolist (elem data)
      (setq idx (1+ idx))
      (add-to-list 'res `(,(format tpl array-name idx) . ,elem)))
    ;; List must be sent in in ascending numerical order. (╯°□°）╯︵ ┻━┻
    (reverse res)))

(defun phab/generate-php-keyed-associative-array-param (array-name data)
  (-let ((res '())
         (tpl "%s[%s]"))
    (dolist (elem data)
      (add-to-list 'res `(,(format tpl array-name (car elem)) . ,(cdr elem))))
    res))

(defun phab/compile-query (query)
  "Convert a sensible emacs data structure QUERY in to something
  that can be passed as :data to a `request'"
  (-filter #'identity
           (-flatten-n 1
                       (list
                        (list (plist-get query :stored-query)) ;; can just be added
                        (phab/generate-php-indexed-associative-array-param "phids" (plist-get query :phids))
                        (phab/generate-php-keyed-associative-array-param "attachments" (plist-get query :attachments))
                        (phab/generate-php-nested-associative-array-params "constraints" (plist-get query :constraints))
                        ))))

(defun phab/build-constraints (&rest pairs)
  (list :constraints pairs))

(defvar phab/demo-query
  '(
    ;; :stored-query ("queryKey" . "active")
    :phids ("phid1" "phid2" "phid3")
           :attachments (("subscribers" . "true"))
           :constraints (("phids" . ("PHID-USER-3r55zlikd6ctuvcvayf7" "PHID-USER-3r55zlikd6ctuvcvayf4" "PHID-USER-3r55zlikd6ctuvcvayf")))
           ))


;;----------------------------------API Calls------------------------------------
(defun phab/make-phabricator-request (endpoint data &optional success-fn sync)
  "Perform a Phabricator API request against ENDPOINT with query DATA.

If SUCCESS-FN is provided, use it as the success callback for the request.

If SYNC is non-nil, perform a synchronous request."

  (-let* ((url (s-join "/" (list phab/phabricator-url endpoint)))
          (data-with-token (if (dotted-pairp data)
                               (list data phab/token-pair)
                             (-snoc data phab/token-pair))))
    (message "%s" data-with-token)
    (request url
             :data data-with-token
             :parser 'json-read
             :success success-fn
             :error #'phab/query-error-fn
             :sync sync
             )))

(defun phab/make-phabricator-request-deferred (endpoint data)
  "Perform a Phabricator API request against ENDPOINT with query DATA.

If SUCCESS-FN is provided, use it as the success callback for the request.

If SYNC is non-nil, perform a synchronous request."
  (-let* ((url (s-join "/" (list phab/phabricator-url endpoint)))
          (data-with-token (if (dotted-pairp data)
                               (list data phab/token-pair)
                             (-snoc data phab/token-pair))))
    (message "%s" data-with-token)
    (request-deferred url
                      :data data-with-token
                      :parser 'json-read
                      :error #'phab/query-error-fn
                      )))

(defmacro phab/make-api-query-fn (fn-name endpoint &optional doc-string success-fn sync)
  `(defun ,fn-name (&optional query success-fn sync)
     ,doc-string
     (-let ((query-struct (phab/resolve-query query))
            (sfn (or success-fn (phab/create-query-success-fn 'phab/query-result))))
       (phab/make-phabricator-request
        ,endpoint
        query-struct
        sfn
        sync
        ))))

(defmacro phab/make-deferred-api-query-fn (fn-name endpoint &optional doc-string)
  `(defun ,fn-name (&optional query success-fn sync)
     ,doc-string
     (-let [query-struct (phab/resolve-query query)]
       (phab/make-phabricator-request-deferred
        ,endpoint
        query-struct
        ))))

;; Differential
(phab/make-api-query-fn
 phab/differential-revision-search
 "differential.revision.search")
(phab/make-deferred-api-query-fn
 phab/differential-revision-search$
 "differential.revision.search")

(phab/make-api-query-fn
 phab/differential-query
 "differential.query"
 "This method is deprecated, but it also the only way to know what repo a diff belongs to? So that's.... fine.")
(phab/make-deferred-api-query-fn
 phab/differential-query$
 "differential.query")

;; Diffusion
(phab/make-api-query-fn
 phab/diffusion-repository-search
 "diffusion.repository.search")
(phab/make-deferred-api-query-fn
 phab/diffusion-repository-search$
 "diffusion.repository.search")

;; User
(phab/make-api-query-fn phab/user-search "user.search")
(phab/make-deferred-api-query-fn phab/user-search$ "user.search")


;;--------------------------------API Utilities----------------------------------
;; Functions that wrap an API call and try to return something sensible

(defvar phab/known-repository-phids
  (make-hash-table))

(defun phab/data-from-response (resp)
  (-let [result (->> resp
                     (request-response-data)
                     (alist-get 'result))]
    (if (vectorp result)
        result
      (alist-get 'data result))))

;; (defvar phab/test-phids '("PHID-DREV-hzpptt3ow3svfsg7omse" "PHID-DREV-fb3syigzmdj2e4dqszba"))
(defun phab/drev-phids-to-repo-names (&rest phids)
  (-let [dif-repo-map (make-hash-table)]
    nil))

;; Make a thing that reports on all open diffs:
;; 1. Find the diffs
;; 2. Resolve repo names, caching the results
;; 3. Dump to a buffer
;; 4. Make it pretty

;; Write a buffer of this format:
;; <Repo Name> -- <Diff ID>: <Diff Title> -- <Author>, opened on <Date>, Last Updated <Date>

;;---------------------------------Lookup Vars-----------------------------------
;; Phab maintains mappings from PHIDs to almost everything
(defvar phab/repo-names (make-hash-table :test 'equal))
(defvar phab/user-names (make-hash-table :test 'equal))
(defvar phab/diff-repo-map (make-hash-table :test 'equal))

(defun phab/make-diff-writer (buf)
  `(lambda (diffs)
     (with-current-buffer ,buf
       (-each diffs
         (lambda (diff)
           (-let [diff-str (format "%s - %s - %s"
                                   (plist-get diff :diff-phid)
                                   (plist-get diff :repo-phid)
                                   (plist-get diff :repo-name))]
             (insert diff-str)
             (newline)))))))

(defun phab/show-active-diffs ()
  (if (hash-table-empty-p phab/repo-names) (phab/populate-repo-name-hash))
  (-let* ((buf (get-buffer-create "*Phab: Active Diffs*"))
          (phids-deferred (deferred:$ (phab/differential-revision-search$)
                            (deferred:nextc it
                              #'phab/data-from-response)
                            (deferred:nextc it
                              (lambda (resp)
                                (list :phids (-map (lambda (res)
                                                     (alist-get 'phid res))
                                                   resp))))))
          (writer (phab/make-diff-writer buf)))
    (phab/resolve-repo-phids-to-names phids-deferred writer)
    (pop-to-buffer buf)))

(defun phab/resolve-repo-phids-to-names (phids-deferred &optional final-fn)
  (-let [finally (or final-fn (lambda (res) (message "Received: %s" res)))]
    ;; Take a deferred that will eventually yield a list of phids
    ;; Query differential.query to get diff-phid/repo-phid mappings
    (deferred:$ phids-deferred
      (deferred:nextc it
        (lambda (phids)
          (phab/differential-query$ phids)))
      (deferred:nextc it
        #'phab/data-from-response)
      (deferred:nextc it
        (lambda (response)
          (-let [rev-phid/repo-phid-pairs
                 (-map (lambda (res)
                         (list
                          :diff-phid (alist-get 'phid res)
                          :repo-phid (alist-get 'repositoryPHID res)))
                       response)]
            rev-phid/repo-phid-pairs)))
      (deferred:nextc it
        (lambda (pairs)
          (-map (lambda (rev-phid/repo-phid)
                  (-let* ((repo-phid (plist-get rev-phid/repo-phid :repo-phid))
                          (repo-name (gethash repo-phid phab/repo-names "n/a")))
                    (plist-put rev-phid/repo-phid :repo-name repo-name)))
                pairs)))
      (deferred:nextc it
        finally))))

(defun phab/populate-repo-name-hash ()
  (deferred:$
    (deferred:next #'phab/diffusion-repository-search$)
    (deferred:nextc it
      #'phab/data-from-response)
    (deferred:nextc it
      (lambda (response)
        (-map
         (lambda (repo-data)
           (-let ((repo-phid (alist-get 'phid repo-data))
                  (repo-name (->> (alist-get 'fields repo-data)
                                  (alist-get 'name))))
             (puthash repo-phid repo-name phab/repo-names)))
         response)))))

(defun phab/populate-user-name-hash ()
  (deferred:$
    (deferred:next #'phab/user-search$)
    (deferred:nextc it
      #'phab/data-from-response)
    (deferred:nextc it
      (lambda (response)
        (-map
         (lambda (user)
           (-let* ((user-phid (alist-get 'phid user))
                   (user-data (alist-get 'fields user))
                   (username (alist-get 'username user-data))
                   (realname (alist-get 'realName user-data))
                   (user-plist `(:username ,username :real-name ,realname)))
             (puthash user-phid user-plist phab/user-names)
             ))
         response)))))
(phab/populate-user-name-hash)
;; (phab/resolve-repo-phids-to-names)

;; (defun phab/repository-resove-phid-to-real-name (phid)
;;   (-let []))
;; ;; I don't know what this function should look like
;; (defun phab/build-query-constraint (key vals)
;;   `(("constraints" . ((,key . (,vals))))))


;; Handy request settings
;; (setq request-log-level 'blather
;;       request-message-level 'blather)


;; A little test chatter
;; (phab/user-search '(("constraints" . (("phids" . ("PHID-USER-3r55zlikd6ctuvcvayf7"))))) (phab/create-query-success-fn 'phab/found-user))

;; (defvar phab/test-params (phab/generate-php-arrays
;;                           '(("constraints" . (("phids" . ("PHID-USER-3r55zlikd6ctuvcvayf7" "PHID-USER-3r55zlikd6ctuvcvayf4" "PHID-USER-3r55zlikd6ctuvcvayf"))))
;;                             ("poot" . (("scoot" . ("cat" "dog")))))))
;; (append phab/test-params phab/token-pair)

(provide 'phabricator)
;;; phabricator.el ends here
